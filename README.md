# Chessboard

#### Supported Command
* King [position]
* Queen [position]
* Bishop [position]
* Horse [position]
* Rook [position]
* Pawn [position]

In case when the command provided doesn't match with the above list of command the response from the application will be empty list

Example Input/Output
````
Input
King D5
Output
[C5, E5, D6, D4, E6, E4, C6, C4]

Input 
Horse E3
Output
[C4, C2, G4, G2, F5, D5, F1, D1]
````

# Dependencies
The application uses junit:4.12 and hamcrest-library:1.3 for unit test.

#### To install the application
Execute the following command
````
mvn install
````

To run the program, `cd` into the folder where the binary is and then execute the below command
````
java -jar chessboard.jar
```` 