package com.chessboard.app;

import com.chessboard.app.game.Game;
import com.chessboard.app.game.PositionParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ChessboardApplication {

  public static void main(final String[] args) throws IOException {
    var positionParser = new PositionParser();
    var reader = new BufferedReader(new InputStreamReader(System.in));

    Game game = new Game(reader, positionParser);
    game.start();
    reader.close();
  }
}
