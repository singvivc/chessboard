package com.chessboard.app.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Board {

  private final int height;
  private final int width;

  public Board(final int height, final int width) {
    this.height = height;
    this.width = width;
  }

  public boolean isValidPosition(final int xPosition, final int yPosition) {
    return (xPosition >= 0 && xPosition < width)
            && (yPosition >= 0 && yPosition < height);
  }

  public List<int[]> jumpTill(final List<Integer> xPosition, final List<Integer> yPosition, final int[] from) {
    return allPositionStream(xPosition, yPosition).map(it -> nextValidPositionOnBoard(from, it))
            .filter(it -> it != null).collect(toList());
  }

  public List<int[]> moveTill(final List<Integer> xPosition, final List<Integer> yPosition, final int[] from) {
    return allPositionStream(xPosition, yPosition).map(it -> till(from, it))
            .flatMap(Collection::stream).filter(it -> it != null).collect(toList());
  }

  private Stream<int[]> allPositionStream(final List<Integer> xPosition, final List<Integer> yPosition) {
    return IntStream.range(0, xPosition.size())
            .mapToObj(index -> new int[] {xPosition.get(index), yPosition.get(index)});
  }

  private List<int[]> till(final int[] from, final int[] moveBy) {
    int[] current = from;
    List<int[]> validMoves = new ArrayList<>();
    while (current != null) {
      int[] nextPosition = nextValidPositionOnBoard(current, moveBy);
      validMoves.add(nextPosition);
      current = nextPosition;
    }
    return validMoves;
  }

  private int[] nextValidPositionOnBoard(final int[] from, final int[] moveBy) {
    final int xPosition = from[0] + moveBy[0];
    final int yPosition = from[1] + moveBy[1];
    if (isValidPosition(xPosition, yPosition)) {
      return new int[] {xPosition, yPosition};
    }
    return null;
  }
}
