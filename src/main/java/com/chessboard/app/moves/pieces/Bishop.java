package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import com.chessboard.app.moves.MovesStrategy;

import java.util.List;

public class Bishop implements MovesStrategy {

  private final List<Integer> xPositions = List.of(1, -1, -1, 1);
  private final List<Integer> yPositions = List.of(1, -1, 1, -1);

  @Override
  public List<int[]> allMoves(final int[] from, final Board board) {
    return board.moveTill(xPositions, yPositions, from);
  }

  @Override
  public String getType() {
    return "Bishop".toUpperCase();
  }
}
