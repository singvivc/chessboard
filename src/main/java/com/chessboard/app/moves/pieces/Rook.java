package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import com.chessboard.app.moves.MovesStrategy;

import java.util.List;

public class Rook implements MovesStrategy {

  private final List<Integer> xPosition = List.of(-1, 1, 0, 0);
  private final List<Integer> yPosition = List.of(0, 0, -1, 1);

  @Override
  public List<int[]> allMoves(final int[] from, final Board board) {
    return board.moveTill(xPosition, yPosition, from);
  }

  @Override
  public String getType() {
    return "Rook".toUpperCase();
  }
}
