package com.chessboard.app.moves;

import com.chessboard.app.board.Board;

import java.util.List;

public interface MovesStrategy {

  public List<int[]> allMoves(final int[] from, final Board board);
  public String getType();
}
