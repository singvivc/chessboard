package com.chessboard.app.moves;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public final class MovesStrategyFactory {

  private final Map<String, MovesStrategy> strategyByType = new HashMap<>();

  private final static MovesStrategyFactory INSTANCE = new MovesStrategyFactory();

  private MovesStrategyFactory() {
    autoCreateStrategyMovesForEachPiece(strategyByType);
  }

  public static final MovesStrategyFactory getInstance() {
    return INSTANCE;
  }

  public MovesStrategy getStrategyByType(final String pieceType) {
    return strategyByType.getOrDefault(pieceType, null);
  }

  private void autoCreateStrategyMovesForEachPiece(final Map<String, MovesStrategy> strategyByType) {
    var strategiesServiceLoader = ServiceLoader.<MovesStrategy>load(MovesStrategy.class);
    strategiesServiceLoader.forEach(service -> strategyByType.put(service.getType().toUpperCase(), service));
  }
}
