package com.chessboard.app.game.exception;

public class InvalidPositionException extends RuntimeException {

  public InvalidPositionException() {
    super();
  }

  public InvalidPositionException(final String errorMessage) {
    super(errorMessage);
  }
}
