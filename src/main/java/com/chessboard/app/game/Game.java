package com.chessboard.app.game;

import com.chessboard.app.board.Board;
import com.chessboard.app.game.exception.InvalidPositionException;
import com.chessboard.app.moves.MovesStrategy;
import com.chessboard.app.moves.MovesStrategyFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Game {

  private final Board board;
  private final PositionParser positionParser;
  private final BufferedReader bufferedReader;

  private final MovesStrategyFactory moveFactory = MovesStrategyFactory.getInstance();

  public Game(final BufferedReader bufferedReader, final PositionParser positionParser) {
    this.board = new Board(8,8);
    this.positionParser = positionParser;
    this.bufferedReader = bufferedReader;
  }

  public List<String> start() {
    try {
      final String input = bufferedReader.readLine();
      String[] tokens = input.split(" ");
      if (tokens.length < 2) {
        throw new IllegalArgumentException("input required length is 2");
      }
      return findPossibleMoves(tokens[0].toUpperCase(), tokens[1].toUpperCase());
    } catch (IOException exception) {
      System.err.println("Exception occurred while reading from console or the input provided is incorrect");
    }
    return Collections.emptyList();
  }

  public List<String> findPossibleMoves(final String pieceType, final String position) {
    final var coordinates = positionParser.parse(position);
    if (!board.isValidPosition(coordinates[0], coordinates[1])) {
      throw new InvalidPositionException("The position of the piece on the board is invalid");
    }
    return findAllMoves(pieceType, coordinates);
  }

  private List<String> findAllMoves(final String pieceType, final int[] coordinates) {
    MovesStrategy movesStrategy = moveFactory.getStrategyByType(pieceType);
    var allPossibleMoves = movesStrategy.allMoves(coordinates, board);
    return allPossibleMoves.stream().map(positionParser::format).collect(Collectors.toList());
  }
}
