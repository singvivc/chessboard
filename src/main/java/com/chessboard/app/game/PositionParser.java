package com.chessboard.app.game;

public class PositionParser {

  public int[] parse(final String position) {
    char[] tokens = position.toCharArray();
    int xPosition = tokens[0] - 'A';
    int yPosition = Integer.parseInt(String.valueOf(tokens[1])) - 1;
    return new int[]{xPosition, yPosition};
  }

  public String format(final int[] coordinates) {
    char row = (char) ('A' + coordinates[0]);
    return String.valueOf(row) + "" + (coordinates[1] + 1);
  }
}
