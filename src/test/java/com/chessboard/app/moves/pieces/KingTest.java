package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;


@RunWith(value = Parameterized.class)
public class KingTest {

  private final Board board;
  private final int[] coordinates;
  private final List<int[]> expectedAnswers;

  public KingTest(final int[] coordinates, final List<int[]> expectedAnswers) {
    this.board = new Board(8, 8);
    this.coordinates = coordinates;
    this.expectedAnswers = expectedAnswers;
  }

  @Parameterized.Parameters(name = "when king is at {0} it can move to {1} possible tiles")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][] {
            {new int[]{5, 6}, List.of(new int[]{6, 7}, new int[]{5, 7}, new int[]{4, 7}, new int[]{4, 6},
                    new int[]{4, 5}, new int[]{5, 5}, new int[]{6, 5}, new int[]{6, 6})},
            {new int[]{7, 7}, List.of(new int[]{6, 7}, new int[]{6, 6}, new int[]{7, 6})},
            {new int[]{0, 0}, List.of(new int[]{0, 1}, new int[]{1, 1}, new int[]{1, 0})},
            {new int[]{3, 0}, List.of(new int[]{2, 0}, new int[]{4, 0}, new int[]{2, 1}, new int[]{3, 1},
                    new int[]{4, 1})},
            {new int[]{3, 4}, List.of(new int[]{3, 5}, new int[]{4, 5}, new int[]{4, 4}, new int[]{4, 3},
                    new int[]{3, 3}, new int[]{2, 3}, new int[]{2, 4}, new int[]{2, 5})},
    });
  }

  @Test
  public void givenValidKingPosition_testShouldReturnAllPossiblePosition() {
    King king = new King();
    var allPositions = king.allMoves(coordinates, board);
    assertThat(allPositions.size(), is(expectedAnswers.size()));
    assertThat(allPositions, containsInAnyOrder(expectedAnswers.toArray()));
  }
}
