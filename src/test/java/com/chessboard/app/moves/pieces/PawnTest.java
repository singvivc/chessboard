package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

@RunWith(value = Parameterized.class)
public class PawnTest {

  private final Board board;
  private final int[] position;
  private final List<int[]> expectedAnswer;

  public PawnTest(final int[] position, final List<int[]> expectedAnswer) {
    board = new Board(8, 8);
    this.position = position;
    this.expectedAnswer = expectedAnswer;
  }

  @Parameterized.Parameters(name = "when pawn is at position {0} it can move to {1} possible positions")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][]{
            {new int[]{4, 2}, List.of(new int[]{4, 3})},
            {new int[]{0, 7}, List.of()},
            {new int[]{0, 0}, List.of(new int[]{0, 1})},
            {new int[]{3, 0}, List.of(new int[]{3, 1})},
            {new int[]{3, 4}, List.of(new int[]{3, 5})}
    });
  }

  @Test
  public void givenValidPawnPositionOnBoard_testShouldReturnAllPossibleMoves() {
    Pawn pawn = new Pawn();
    var possiblePositions = pawn.allMoves(position, board);
    assertThat(possiblePositions.size(), is(expectedAnswer.size()));
    assertThat(possiblePositions, containsInAnyOrder(expectedAnswer.toArray()));
  }
}
