package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

@RunWith(value = Parameterized.class)
public class RookTest {

  private final Board board;
  private final int[] position;
  private final List<int[]> expectedAnswer;

  public RookTest(final int[] position, final List<int[]> expectedAnswer) {
    board = new Board(8, 8);
    this.position = position;
    this.expectedAnswer = expectedAnswer;
  }


  @Parameterized.Parameters(name = "when rook is at position {0} it can move to {1} possible positions")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][]{
            {new int[]{4, 2}, List.of(new int[]{3, 2}, new int[]{2, 2}, new int[]{1, 2}, new int[]{0, 2},
                    new int[]{5, 2}, new int[]{6, 2}, new int[]{7, 2}, new int[]{4, 1}, new int[]{4, 0},
                    new int[]{4, 3}, new int[]{4, 4}, new int[]{4, 5}, new int[]{4, 6}, new int[]{4, 7})},
            {new int[]{0, 7}, List.of(new int[]{1, 7}, new int[]{2, 7}, new int[]{3, 7}, new int[]{4, 7},
                    new int[]{5, 7}, new int[]{6, 7}, new int[]{7, 7}, new int[]{0, 6}, new int[]{0, 5},
                    new int[]{0, 4}, new int[]{0, 3}, new int[]{0, 2}, new int[]{0, 1}, new int[]{0, 0})},
            {new int[]{0, 0}, List.of(new int[]{1, 0}, new int[]{2, 0}, new int[]{3, 0}, new int[]{4, 0},
                    new int[]{5, 0}, new int[]{6, 0}, new int[]{7, 0}, new int[]{0, 1}, new int[]{0, 2},
                    new int[]{0, 3}, new int[]{0, 4}, new int[]{0, 5}, new int[]{0, 6}, new int[]{0, 7})},
            {new int[]{3, 0}, List.of(new int[]{2, 0}, new int[]{1, 0}, new int[]{0, 0}, new int[]{4, 0},
                    new int[]{5, 0}, new int[]{6, 0}, new int[]{7, 0}, new int[]{3, 1}, new int[]{3, 2},
                    new int[]{3, 3}, new int[]{3, 4}, new int[]{3, 5}, new int[]{3, 6}, new int[]{3, 7})},
            {new int[]{3, 4}, List.of(new int[]{2, 4}, new int[]{1, 4}, new int[]{0, 4}, new int[]{4, 4},
                    new int[]{5, 4}, new int[]{6, 4}, new int[]{7, 4}, new int[]{3, 3}, new int[]{3, 2},
                    new int[]{3, 1}, new int[]{3, 0}, new int[]{3, 5}, new int[]{3, 6}, new int[]{3, 7})}
    });
  }

  @Test
  public void givenValidPawnPositionOnBoard_testShouldReturnAllPossibleMoves() {
    Rook rook = new Rook();
    var possiblePositions = rook.allMoves(position, board);
    assertThat(possiblePositions.size(), is(expectedAnswer.size()));
    assertThat(possiblePositions, containsInAnyOrder(expectedAnswer.toArray()));
  }
}
