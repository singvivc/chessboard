package com.chessboard.app.moves.pieces;

import com.chessboard.app.board.Board;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

@RunWith(value = Parameterized.class)
public class HorseTest {

  private final Board board;
  private final int[] position;
  private final List<int[]> expectedAnswer;

  public HorseTest(final int[] position, final List<int[]> expectedAnswer) {
    board = new Board(8, 8);
    this.position = position;
    this.expectedAnswer = expectedAnswer;
  }

  @Parameterized.Parameters(name = "when horse is at position {0} it can move to {1} possible positions")
  public static Iterable<Object[]> data() {
    return Arrays.asList(new Object[][]{
            {new int[]{4, 2}, List.of(new int[]{2, 3}, new int[]{2, 1}, new int[]{6, 3}, new int[]{6, 1},
                    new int[]{5, 4}, new int[]{3, 4}, new int[]{5, 0}, new int[]{3, 0})},
            {new int[]{0, 7}, List.of(new int[]{2, 6}, new int[]{1, 5})},
            {new int[]{0, 0}, List.of(new int[]{2, 1}, new int[]{1, 2})},
            {new int[]{3, 0}, List.of(new int[]{1, 1}, new int[]{5, 1}, new int[]{4, 2}, new int[]{2, 2})},
            {new int[]{3, 4}, List.of(new int[]{1, 5}, new int[]{1, 3}, new int[]{5, 5}, new int[]{5, 3},
                    new int[]{4, 6}, new int[]{2, 6}, new int[]{4, 2}, new int[]{2, 2})}
    });
  }

  @Test
  public void givenValidHorsePositionOnBoard_testShouldReturnAllPossibleMoves() {
    Horse horse = new Horse();
    var possiblePositions = horse.allMoves(position, board);
    assertThat(possiblePositions.size(), is(expectedAnswer.size()));
    assertThat(possiblePositions, containsInAnyOrder(expectedAnswer.toArray()));
  }
}
