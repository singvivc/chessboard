package com.chessboard.app.game;

import com.chessboard.app.game.exception.InvalidPositionException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

@ExtendWith(MockitoExtension.class)
public class GameTest {

  private static BufferedReader bufferedReader;

  private static Game game;

  @BeforeClass
  public static void setUp() {
    bufferedReader = mock(BufferedReader.class);
    game = new Game(bufferedReader, new PositionParser());
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossibleKingsMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("King D5");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(8));
    assertThat(possibleMoves, containsInAnyOrder("C5", "E5", "D6", "D4", "E6", "E4", "C6", "C4"));
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossibleQueensMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("Queen D5");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(27));
    assertThat(possibleMoves, containsInAnyOrder("C5", "B5", "A5", "E5", "F5", "G5", "H5", "D6", "D7", "D8",
            "D4", "D3", "D2", "D1", "E6", "F7", "G8", "C4", "B3", "A2", "C6", "B7", "A8", "E4", "F3", "G2", "H1"));
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossibleHorseMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("Horse E3");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(8));
    assertThat(possibleMoves, containsInAnyOrder("C4", "D5", "C2", "G4", "G2", "F5", "F1", "D1"));
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossibleBishopMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("Bishop E3");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(11));
    assertThat(possibleMoves, containsInAnyOrder("G1", "H6", "G5", "B6", "A7", "D2", "F4", "F2", "D4", "C1",
            "C5"));
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossiblePawnMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("Pawn F1");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(1));
    assertThat(possibleMoves, containsInAnyOrder("F2"));
  }

  @Test
  public void givenValidInputFromConsole_testShouldVerifyTheReturnOfAllPossibleRookMoves() throws Exception {
    when(bufferedReader.readLine()).thenReturn("Rook F1");
    List<String> possibleMoves = game.start();

    assertThat(possibleMoves.size(), is(14));
    assertThat(possibleMoves, containsInAnyOrder("H1", "C1", "F8", "F4", "F2", "F7", "B1", "E1", "A1", "G1",
            "F3", "F5", "D1", "F6"));
  }

  @Test
  public void givenInvalidInputFromConsole_testShouldThrowInvalidArgumentException() throws Exception {
    when(bufferedReader.readLine()).thenReturn("King");
    var exception = assertThrows(IllegalArgumentException.class, () -> game.start());

    assertThat(exception, notNullValue());
    assertThat(exception.getMessage(), equalTo("input required length is 2"));
  }

  @Test
  public void givenInvalidPiecePositionOnBoard_testShouldThrowInvalidPositionException() throws Exception {
    when(bufferedReader.readLine()).thenReturn("King H9");
    var exception = assertThrows(InvalidPositionException.class, () -> game.start());

    assertThat(exception, notNullValue());
    assertThat(exception.getMessage(), equalTo("The position of the piece on the board is invalid"));
  }
}
